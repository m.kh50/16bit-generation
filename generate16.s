        .global generate16

generate16:                     

        push    {LR}
        push    {R1-R12}                @ saves the content of LR and R1-R12
       
        mov     R1, #0
        mov     R2, #0                  @ resets all registers to 0
        mov     R3, R0                  @ initializes registers
        mov     R0, #0
        mov     R4, #1
        mov     R5, #0
        mov     R6, #15
        mov     R7, #0
        mov     R8, #0
        mov     R9, #0
        mov     R10, #0
        mov     R11, #0
        mov     R12, #0

display_15to11:                         @ displays bits from 15 to 11 in R0
        and     R2, R4, R3, lsr R6
                                        @ also calculates parity bit 
        cmp     R2, #1                  @ at position 15
        addeq   R1, #1

        cmp     R6, #11
        addge   R0, R2
        lslge   R0, #1
        subge   R6, #1
        bgt     display_15to11
        
        bl      modulus

display_10to4:                          @ displays bits from 10 to 4 in R0
        and     R2, R4, R3, lsr R6
                                        @ also calculates parity bit
        cmp     R2, #1                  @ at position 7
        addeq   R1, #1

        cmp     R6, #4
        addge   R0, R2
        lslge   R0, #1
        subge   R6, #1
        bgt     display_10to4

        bl      modulus
        mov     R6, #3

display_3to1:                           @ displays bits from 10 to 4 in R0
        and     R2, R4, R3, lsr R6

        cmp     R6, #1
        addge   R0, R2
        lslge   R0, #1
        subge   R6, #1
        bgt     display_3to1
        mov     R6, #15

p4:                                     @ calculates parity bit 4
        and     R2, R4, R3, lsr R6

        cmp     R2, #1
        addeq   R1, #1

        cmp     R6, #14
        subeq   R6, #4
        beq     p4

        cmp     R6, #7
        subeq   R6, #4
        beq     p4

        sub     R6, #1
        cmp     R6, #1
        bge     p4

        bl      modulus
        mov     R6, #0

display_0:                              @ displays last bit
        and     R2, R4, R3, lsr R6
        add     R0, R2
        lsl     R0, #1
        mov     R6, #13

p2:                                     @ calculates parity bit 2
        and     R2, R4, R3, lsr R6

        cmp     R2, #1
        addeq   R1, #1

        cmp     R6, #12
        subeq   R6, #2
        beq     p2

        cmp     R6, #9
        subeq   R6, #3
        beq     p2

        cmp     R6, #5
        subeq   R6, #2
        beq     p2

        cmp     R6, #2
        subeq   R6, #2
        beq     p2  

        subs    R6, #1
        bge     p2

        bl      modulus 
        mov     R6, #15

p1:                                     @ calculates parity bit 1
        and     R2, R4, R3, lsr R6

        cmp     R2, #1
        addeq   R1, #1

        cmp     R6, #11
        subeq   R6, #1
        beq     p1

        cmp     R6, #4
        subeq   R6, #1
        beq     p1

        cmp     R6, #1
        subeq   R6, #1
        beq     p1

        subs    R6, #2
        bge     p1

        bl      modulus

        lsr     R0, #1

        pop     {R1-R12}   
        pop     {LR}   

        bx      LR


modulus:                 @ tells whether the number of 1s was even or odd
        
        subs    R1, #2
        bgt     modulus

        cmp     R1, #0
        addne   R0, #1
        lsl     R0, #1
        movne   R1, #0

        bx      LR


